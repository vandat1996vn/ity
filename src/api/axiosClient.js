import axios from 'axios';

const axiosClient = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}/api`,
  timeout: 10000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

// Add a request interceptor
axiosClient.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    localStorage.setItem('loadding', true);
    const token = localStorage.getItem('access_token');
    // const token = process.env.TOKEN;
    config.headers.Authorization = token ? 'Token ' + localStorage.getItem('access_token') : '';
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

axiosClient.interceptors.response.use(
  (response) => {
    localStorage.setItem('loadding', false);
    return response;
  },
  (error) => {
    const originalRequest = error.config;

    if (
      error.response.status === 401 &&
      error.response.data.detail === 'Token is invalid or expired'
      // error.response.data.detail === 'User not found'
    ) {
      localStorage.clear();
      window.location.href = '/login';
      return Promise.reject(error);
    }
    if (
      error.response.status === 401 &&
      error.response.data.detail === 'Authentication credentials were not provided.'
      // error.response.data.detail === 'User not found'
    ) {
      localStorage.clear();
      window.location.href = '/login';
      return Promise.reject(error);
    }

    localStorage.setItem('loadding', false);
    return Promise.reject(error);
  }
);

export default axiosClient;
