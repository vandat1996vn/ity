import React, { useRef, useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { narBar } from "../../util/constan";
import { useDispatch } from "react-redux";
import { getProfile } from "../../features/user/userSlice";
import { useSelector } from "react-redux";
import { selectProfile } from "../../features/user/userSlice";
import { FaUserCircle } from "react-icons/fa";
import { BiCaretDown } from "react-icons/bi";
import logoMain from "../../../src/assets/images/logo-vntel.png";

const Header = ({ menu }) => {
  const headerRef = useRef(null);
  const profileHeader = useSelector(selectProfile);
  const [active, setActive] = useState(false);
  const location = useLocation();
  const dispatch = useDispatch();

  const [profile, setProfile] = useState();
  const [hide, setHide] = useState(true);

  console.log("profile", profileHeader);

  const activeRoute = (routeName) => {
    if (routeName) {
      return location.pathname.includes(routeName);
    }
    return false;
  };

  const handleOpenBtn = (event) => {
    setActive(active === false ? true : false);
    event.stopPropagation();
  };

  useEffect(() => {
    dispatch(getProfile())
      .unwrap()
      .then((res) => {
        setProfile(res?.data);
      });
  }, []);

  useEffect(() => {
    function myFunction() {
      setActive(false);
    }

    document.addEventListener("click", myFunction);
  });

  const logout = async () => {
    await localStorage.clear();
    window.location.reload();
  };

  return (
    <div className="header-container" ref={headerRef}>
      <header>
        <div className="container">
          <div className="logo-header__main">
            <img src={logoMain} />
          </div>
          <ul
            className="menu-main"
            style={{

            }}
          >
            {narBar?.map((v, i) => {
              return profile?.isStaff === true
                ? v?.staff.includes("STAFF") && v?.staff.includes("USER") && (
                    <li
                      className={`menu-item menu-single ${
                        activeRoute(v?.path) ? "active" : ""
                      }`}
                      key={i}
                    >
                      <Link
                        to={v.path}
                        className="menu-toggle"
                        style={{
                          height: "40px",
                          justifyContent: !hide && "center",
                          backgroundColor: hide && activeRoute(v.path)
                        }}
                      >
                        <span className="inline-flex ml-4 items-center">
                          {v.svg}
                          {hide && (
                            <span>
                              {v.icons} {v.name}
                            </span>
                          )}
                        </span>
                      </Link>
                    </li>
                  )
                : v?.staff.includes("ALL") && v?.staff.includes("USER") && (
                    <li
                      className={`menu-item menu-single ${
                        activeRoute(v?.path) ? "active" : ""
                      }`}
                      key={i}
                    >
                      <Link
                        to={v.path}
                        className="menu-toggle"
                        style={{
                          height: "40px",
                          justifyContent: !hide && "center",
                          backgroundColor: hide && activeRoute(v.path)
                        }}
                      >
                        <span className="inline-flex ml-4 items-center">
                          {v.svg}
                          {hide && (
                            <span>
                              {v.icons} {v.name}
                            </span>
                          )}
                        </span>
                      </Link>
                    </li>
                  );
            })}
          </ul>
          <div className="box-user__login">
            <h3 className="name-user__login">
              {profileHeader?.data?.username}
            </h3>
            <div
              className={`btn-avatar__header ${
                active === true ? "active" : ""
              } `}
              onClick={(e) => handleOpenBtn(e)}
            >
              <div className="avatar-user__login">
                {profileHeader?.data?.avatar ? (
                  <img src={profileHeader?.data?.avatar} alt="" />
                ) : (
                  <FaUserCircle />
                )}
              </div>
              <BiCaretDown />
              <ul className="btn-user__header">
                <li>
                  <p onClick={() => logout()}>Đăng xuất</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </div>
  );
};

export default Header;
