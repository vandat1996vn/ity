import React, { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import Navbar from "../narbar/narbar";
import Header from "./Header";
import Footer from "../Footer/Footer";
import "./admin.css";
import { useDispatch } from "react-redux";
function Admin() {
  const [menu, setMenu] = useState(true);
  const dispatch = useDispatch();
  return (
    <>
      <Header />
      <div className="contain-page">
        <div className="container">
          <Outlet />
        </div>
      </div>
      <Footer />
    </>
  );
}

export default Admin;
