import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

const Footer = () => {
  var CurrentYear = new Date().getFullYear();

  return (
    <footer className="footer">
      <div className="container">
        Copyright © <span id="year">{CurrentYear} </span>
      </div>
    </footer>
  );
};

export default Footer;
