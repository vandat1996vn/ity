import React, { useEffect, useState } from "react";
import { getFollow } from "../../../features/follow/followSlide";
import { useDispatch } from "react-redux";
import Loading from "../../../components/common/Loading";

const FollowOnline = () => {
  const dispatch = useDispatch();
  const [followOnline, setFollowOnline] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    dispatch(getFollow())
      .unwrap()
      .then((res) => {
        setLoading(true);
        setFollowOnline(res?.data?.results);
      });
  }, []);

  let listFollowOnline = followOnline?.map((item) => {
    return {
      extension: item.ext,
      connected: item.userAgent,
      avatar: item.avatar
    };
  });

  return (
    <>
      <div className="box-content">
        <div className="top-record__phone">
          <h3 className="tittle-tops">Theo dõi online</h3>
        </div>
        {loading === true ? (
          <div className="list-follow">
            <div className="row">
              {listFollowOnline?.map((item, index) => (
                <div className="col-lg-2" key={index}>
                  <div
                    className={`items-follow__online ${
                      item.connected !== null ? "item-online" : "item-offline"
                    }`}
                  >
                    <div className="intros-follow__online">
                      <div className="tops-items__follow">
                        <div
                          className={`avatar-item__follow ${
                            !item.avatar === null
                              ? "avatar-follow"
                              : "avatar-icons"
                          }`}
                        >
                          <span className="icon-avatar__follows"></span>
                          <img src={item.avatar} alt=""></img>
                          <i className="fa fa-user-o" aria-hidden="true"></i>
                        </div>
                        <h3 className="names-follow__online">
                          {item.extension}
                        </h3>
                      </div>
                      <p className="status-follow__online">
                        {item.connected !== null
                          ? "Đang hoạt động"
                          : "không hoạt động"}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          <Loading />
        )}
      </div>
    </>
  );
};

export default FollowOnline;
