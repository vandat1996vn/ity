import React, { useState, useEffect } from "react";
import { BiArrowBack } from "react-icons/bi";
import {
  getDashboard,
  getDashboardDay
} from "../../../features/dashboard/dashboardSlice";
import { useDispatch } from "react-redux";
import Loading from "../../../components/common/Loading";

const Dashboard = () => {
  const dispatch = useDispatch();
  const [infoDay, setInfoDay] = useState({});
  const [infoMonth, setInfoMonth] = useState({});
  const [loading, setLoading] = useState(false);
  const [directions, setDirections] = useState("out");

  useEffect(() => {
    dispatch(getDashboard({ direction: directions }))
      .unwrap()
      .then((res) => {
        setLoading(true);
        setInfoMonth(res?.data?.month);
      });
  }, [directions]);

  useEffect(() => {
    dispatch(getDashboardDay({ direction: directions }))
      .unwrap()
      .then((res) => {
        setLoading(true);
        setInfoDay(res?.data?.day);
      });
  }, [directions]);

  console.log("logDay", infoDay);

  return (
    <>
      <div className="box-content">
        <div className="top-record__phone">
          <h3 className="tittle-tops">Dashboard</h3>
        </div>
        <div className="in-out__sever">
          <div
            className={`check-box__alls ${
              directions === "in" ? "active-in__out" : ""
            }`}
          >
            <input
              type="radio"
              className="form-check-input"
              onClick={() => setDirections("in")}
              id="in"
              name="direction"
            />
            <span className="checkmark"></span>
            <label>Cuộc gọi đến</label>
          </div>
          <div
            className={`check-box__alls ${
              directions === "out" ? "active-in__out" : ""
            }`}
          >
            <input
              type="radio"
              className="form-check-input"
              onClick={() => setDirections("out")}
              id="out"
              name="direction"
            />
            <span className="checkmark"> </span>
            <label>Cuộc gọi đi</label>
          </div>
        </div>
        {loading === true ? (
          <div className="content-dashboard">
            <div className="list-info__dashboard">
              <div className="row gutter-10">
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoMonth?.total !== null
                        ? infoMonth?.total?.toLocaleString("en-US")
                        : "0"}
                    </p>
                    <p className="text-item__dashboards">
                      Tổng cuộc gọi trong tháng
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoMonth?.totalAnwser !== null
                        ? infoMonth?.totalAnwser?.toLocaleString("en-US")
                        : "0"}
                    </p>
                    <p className="text-item__dashboards">
                      Tổng cuộc gọi nghe máy trong tháng
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoMonth?.totalTime !== null
                        ? infoMonth?.totalTime
                        : "0"}
                    </p>
                    <p className="text-item__dashboards">
                      Tổng số phút gọi trong tháng
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoMonth?.totalTimeAnwser !== null
                        ? infoMonth?.totalTimeAnwser
                        : "0"}
                    </p>
                    <p className="text-item__dashboards">
                      Tổng số phút nghe máy trong tháng
                    </p>
                  </div>
                </div>
              </div>
              <div className="row gutter-10">
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoDay?.total !== null
                        ? infoDay?.total?.toLocaleString("en-US")
                        : "0"}
                    </p>
                    <p className="text-item__dashboards">
                      Tổng cuộc gọi trong ngày
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoDay?.totalAnwser !== null
                        ? infoDay?.totalAnwser?.toLocaleString("en-US")
                        : "0"}
                    </p>
                    <p className="text-item__dashboards">
                      Tổng cuộc gọi nghe máy trong ngày
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoDay?.incomingTime !== null
                        ? infoDay?.totalTime
                        : "0"}
                    </p>
                    <p className="text-item__dashboards">
                      Tổng số phút gọi trong ngày
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoDay?.totalTimeAnwser !== null
                        ? infoDay?.totalTimeAnwser
                        : "0"}
                    </p>
                    <p className="text-item__dashboards">
                      Tổng số phút nghe máy trong ngày
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <Loading />
        )}
      </div>
    </>
  );
};

export default Dashboard;
