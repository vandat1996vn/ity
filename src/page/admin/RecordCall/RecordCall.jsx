import React, { useEffect, useState, useRef } from "react";
import { getRecord, getDowAudio } from "../../../features/record/recordSlide";
import { useDispatch } from "react-redux";
import moment from "moment";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Pagination } from "../../../components/common/Pagination";
import { usePaginationState } from "../../../hooks/use-pagination";
import { FaPhoneSlash, FaPhone } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import Loading from "../../../components/common/Loading";

import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";

const RecordCall = () => {
  const dispatch = useDispatch();

  const [recordCall, getRecordCall] = useState([]);
  const [recordExport, getRecordExport] = useState([]);
  const [phone, setPhone] = useState("");
  const [audio, setAudio] = useState([]);
  const [checkAll, setCheckAll] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loadingReport, setloadingReport] = useState(true);
  const pagination = usePaginationState();
  const inputTable = document.querySelectorAll(".check-down__record");
  const inputCheckAll = document.getElementById("input-change__all");
  const [directions, setDirections] = useState("out");
  const [dateFrom, setDateFrom] = useState(moment(new Date().toJSON()).format(
    "YYYY-MM-DD HH:mm:ss"
  ));
  const [dateTo, setDateTo] = useState(moment(new Date().toJSON()).format(
    "YYYY-MM-DD HH:mm:ss"
  ));
  const [filter, setFilter] = useState({from_dt: dateFrom , to_dt: dateTo});





  useEffect(() => {
    setLoading(false);
    dispatch(
      getRecord({
        direction: directions,
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage
      })
    )
      .unwrap()
      .then((res) => {
        setLoading(true);
        getRecordCall(res?.data);

        inputTable.forEach((item) => {
          item.removeAttribute("checked");
        });
        setAudio([]);
        setCheckAll(false);
        inputCheckAll.checked = false;
      });
  }, [pagination, directions]);

  const handelFilter = (e) => {
    setFilter({ ...filter, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    if (filter?.phone === "") {
      delete filter?.phone;
    }
  }, [filter?.phone]);

  console.log("filter", filter);

  const titleTable = [
    "Ngày gọi",
    "Tên NV",
    "Nguồn",
    "Tới",
    "Điều hướng",
    "Trạng thái",
    "Thời lượng",
    "Đổ chuông",
    "Ghi âm"
  ];

  const listRecord = recordCall?.results?.map((item) => {
    return {
      names: item.name,
      source: item.callerid,
      destination: item.destination,
      direction: item.direction,
      dstExtension: item.dstExtension,
      status: item.attended,
      durations: item.duration,
      billsec: item.ringing,
      recording: item.uuid,
      date: item.calldate.replace("T", "/")
    };
  });

  let exportReport = recordExport?.results?.map((item) => {
    return {
      names: item.name,
      source: item.callerid,
      destination: item.destination,
      direction: item.direction,
      dstExtension: item.dstExtension,
      status: item.attended,
      durations: item.duration,
      billsec: item.ringing,
      date: item.calldate.replace("T", "/"),
      recording: `https://cms.siptrunk.vn:1443/records/?action=record&id=${item.uuid}`
    };
  });

  console.log("listRecord", listRecord);

  const handleSearch = () => {
    setLoading(false);
    if (phone) {
      if (/^[0-9]+$/.test(phone)) {
      } else {
        toast.error("Số điện thoại phải là số");
      }
    }
    dispatch(
      getRecord(
        {
          direction: directions,
          ...filter,
          limit: pagination.perPage,
          offset: pagination.perPage * pagination.page - pagination.perPage
        },
        [pagination, directions]
      )
    )
      .unwrap()
      .then((res) => {
        setLoading(true);
        getRecordCall(res?.data);
      });
  };

  const handleChangeAudio = (e, item) => {
    if (e.target.checked === true) {
      setAudio([...audio, item.recording]);
    } else {
      setAudio(audio.filter((it) => it !== item.recording));
    }
  };

  const handleChangeAllAudio = (e) => {
    if (e.target.checked === true) {
      // inputTable.forEach((item) => {
      //   item.setAttribute("checked", "");
      // });
      setCheckAll(true);
      setAudio([]);
    } else {
      // inputTable.forEach((item) => {
      //   item.removeAttribute("checked");
      // });
      setCheckAll(false);
    }
  };

  console.log("RecordExport", recordExport);

  useEffect(() => {
    if (checkAll === true) {
      inputTable.forEach((item) => {
        item.setAttribute("checked", "");
        item.checked = true;
      });
      setAudio([...listRecord.map((it) => it.recording)]);
    } else {
      inputTable.forEach((item) => {
        item.removeAttribute("checked");
        item.checked = false;
      });
      setAudio([]);
    }
  }, [checkAll]);

  const dowloadRecord = () => {
    if (audio.length > 0) {
      dispatch(
        getDowAudio({
          uuids: `${audio}`
        })
      )
        .unwrap()
        .then(
          // window.location.href = `https://api.siptrunk.vn/api/download-zip/?uuids=${audio}`
          window.open(
            `https://api.siptrunk.vn/api/download-zip/?uuids=${audio}`,
            "_blank"
          )
        );
    } else {
      toast.error("bạn chưa chọn bản ghi âm");
    }
  };

  const createCSV = (array) => {
    var keys = Object.keys(array[0]);

    var result = "";
    result += keys.join(",");
    result += "\n";

    array.forEach(function (item) {
      keys.forEach(function (key) {
        result += item[key] + ",";
      });
      result += "\n";
    });

    return result;
  };

  const handeExportData = () => {
    setloadingReport(false);
    dispatch(
      getRecord({
        ...filter,
        all: "all"
      })
    )
      .unwrap()
      .then((res) => {
        getRecordExport(res?.data);
        setloadingReport(true);
      })
      .catch((error) => {
        setloadingReport(true);
        toast.error("Export Error");
      });
  };

  const downloadCSV = (array) => {
    let csv = "data:text/csv;charset=utf-8," + createCSV(array);
    let excel = encodeURI(csv);

    let link = document.createElement("a");
    link.setAttribute("href", excel);
    link.setAttribute(
      "download",
      `cdr_${new Date().toJSON().slice(0, 10)}.csv`
    );
    link.click();
  };


  return (
    <>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <div className="box-content">
        <br></br>
        <div className="top-record__phone">
          <h3 className="tittle-tops">Chi tiết cuộc gọi</h3>
          <form>
            <div className="groups-filter__alls">
              <div className="groups-input__alls">
                <p className="label-input__alls">Từ ngày:</p>
                <Datetime
                  value={dateFrom}
                  dateFormat="YYYY-MM-DD"
                  timeFormat="HH:mm:ss"
                  className="start-date"
                  onChange={(value) => {
                    filter["from_dt"] = moment(value).format(
                      "YYYY-MM-DD HH:mm:ss"
                    );
                    setFilter({ ...filter });
                    setDateFrom(filter.dateForm)
                  }}
                />
                {/* <input
                  type="date"
                  className="control-alls input-alls input-date start-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  name="from_dt"
                  onChange={(e) => {
                    handelFilter(e);
                    setStartDate(e.target.value);
                  }}
                /> */}
              </div>
              <div className="groups-input__alls">
                <p className="label-input__alls">Đến ngày</p>
                <Datetime
                  value={dateTo}
                  dateFormat="YYYY-MM-DD"
                  timeFormat="HH:mm:ss"
                  className="end-date"
                  onChange={(value) => {
                    filter["to_dt"] = moment(value).format(
                      "YYYY-MM-DD HH:mm:ss"
                    );
                    setFilter({ ...filter });
                    setDateTo(filter.dateTo)
                  }}
                />
                {/* <input
                  type="date"
                  className="control-alls input-alls input-date end-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  name="to_dt"
                  min={startDate}
                  onChange={(e) => handelFilter(e)}
                /> */}
              </div>
              <div className="groups-input__alls">
                <input
                  type="text"
                  name="phone"
                  className="control-alls input-alls"
                  placeholder="Nguồn và số điện thoại"
                  onChange={(e) => {
                    handelFilter(e);
                    setPhone(e.target.value);
                  }}
                />
              </div>
              <button
                type="submit"
                className="btn-blue__alls"
                value="Tìm kiếm"
                onClick={(event) => {
                  event.preventDefault();
                  handleSearch();
                }}
              >
                Tìm kiếm
              </button>
              <button
                className="btn-dowload__all"
                onClick={(event) => {
                  event.preventDefault();
                  dowloadRecord();
                }}
              >
                Tải xuống file ghi âm
              </button>
            </div>
          </form>
        </div>
        <div className="check-phone__report">
          <div className="in-out__sever">
            <div
              className={`check-box__alls ${
                directions === "in" ? "active-in__out" : ""
              }`}
            >
              <input
                type="radio"
                className="form-check-input"
                onClick={() => setDirections("in")}
                id="in"
                name="direction"
              />
              <span className="checkmark"></span>
              <label>Cuộc gọi đến</label>
            </div>
            <div
              className={`check-box__alls ${
                directions === "out" ? "active-in__out" : ""
              }`}
            >
              <input
                type="radio"
                className="form-check-input"
                onClick={() => setDirections("out")}
                id="out"
                name="direction"
              />
              <span className="checkmark"> </span>
              <label>Cuộc gọi đi</label>
            </div>
          </div>
          <p
            className={`export-file__btn ${
              loadingReport === false ? "report-loading" : ""
            }`}
            onClick={() => {
              handeExportData();
              downloadCSV(exportReport);
            }}
          >
            <i className="fa fa-download" aria-hidden="true"></i> Export
          </p>
        </div>

        {loading === true ? (
          <div className="list-record table-list__all">
            <table className="table-alls">
              <tbody>
                <tr>
                  <td>
                    <div className="check-box__alls">
                      <input
                        type="checkbox"
                        className="form-check-input"
                        id="input-change__all"
                        onChange={(e) => handleChangeAllAudio(e)}
                      />
                      <span className="checkmark"> </span>
                    </div>
                  </td>
                  {titleTable?.map((item, index) => (
                    <td key={index}>{item}</td>
                  ))}
                </tr>
                {listRecord?.map((item, index) => (
                  <tr key={index}>
                    <td>
                      <div className="check-box__alls">
                        <input
                          type="checkbox"
                          className="form-check-input check-down__record"
                          onChange={(input) => handleChangeAudio(input, item)}
                        />
                        <span className="checkmark"> </span>
                      </div>
                    </td>
                    <td>
                      <p className="date-call">{item.date}</p>
                    </td>
                    <td>
                      <p className="name-call">{item.names}</p>
                    </td>
                    <td>
                      <p>{item.source}</p>
                    </td>
                    <td>
                      <p>
                        {directions === "out"
                          ? item.destination
                          : item.dstExtension}
                      </p>
                    </td>
                    <td>
                      <p>{item.direction}</p>
                    </td>
                    <td>
                      <p className={item.status ? "not-call" : "on-call"}>
                        {item.status !== 0 ? <FaPhone /> : <FaPhoneSlash />}
                      </p>
                    </td>
                    <td>
                      <p>{item.durations}</p>
                    </td>
                    <td>
                      <p>{item.billsec}</p>
                    </td>
                    <td>
                      <audio
                        controls
                        src={`https://cms.siptrunk.vn:1443/records/?action=record&id=${item.recording}`}
                      >
                        <a
                          href={`https://cms.siptrunk.vn:1443/records/?action=record&id=${item.recording}`}
                        >
                          Download audio
                        </a>
                      </audio>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ) : (
          <Loading />
        )}
        {loading === false ? (
          // "Không có dữ liệu"
          ""
        ) : (
          <Pagination
            currentPage={pagination.page}
            pageSize={pagination.perPage}
            lastPage={Math.min(
              Math.ceil(
                (recordCall.results ? recordCall?.count : 0) /
                  pagination.perPage
              ),
              Math.ceil(
                (recordCall.results ? recordCall?.count : 0) /
                  pagination.perPage
              )
            )}
            onChangePage={pagination.setPage}
            onChangePageSize={pagination.setPerPage}
            onGoToLast={() =>
              pagination.setPage(
                Math.ceil(
                  (recordCall.results ? recordCall?.count : 0) /
                    pagination.perPage
                )
              )
            }
          />
        )}
      </div>
    </>
  );
};

export default RecordCall;
