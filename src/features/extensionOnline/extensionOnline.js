import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../api/axiosClient';

//////initial
const initialState = {
  loading: false,
  listInfomation: {},
};
// Actions
const ACTION = {
  GET_RECORD: 'api/getExtOnline',
};

export const getExtOnline = createAsyncThunk(ACTION.GET_RECORD, async (body) => {
  return axiosClient.get('/gws-log/', { params: body });
});

const extOnlineSlice = createSlice({
  name: 'api',
  initialState: initialState,
  reducers: {
    Infomation: (state, action) => {
      state.listInfomation = action?.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getExtOnline.pending, (state) => {
        state.loading = true;
      })
      .addCase(getExtOnline.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getExtOnline.fulfilled, (state, action) => {
        state.success = false;
        // state.ticketList = action.payload;
      })
  },
});

export const { Infomation } = extOnlineSlice.actions;

const { reducer: extOnlineReducer } = extOnlineSlice;
export default extOnlineReducer;
