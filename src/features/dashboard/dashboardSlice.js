import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axiosClient from "../../api/axiosClient";

//////initial
const initialState = {
  loading: false,
  listInfomation: {}
};
// Actions
const ACTION = {
  GET_DASHBOARD: "api/getDashboard",
  GET_DASHBOARD_DAY: "api/getDashboard"
};

export const getDashboard = createAsyncThunk(
  ACTION.GET_DASHBOARD,
  async (body) => {
    return axiosClient.get("/dashboard/", { params: body });
  }
);

export const getDashboardDay = createAsyncThunk(
  ACTION.GET_DASHBOARD_DAY,
  async (body) => {
    return axiosClient.get("/dashboard-day/", { params: body });
  }
);

const dashboardSlide = createSlice({
  name: "api",
  initialState: initialState,
  reducers: {
    Infomation: (state, action) => {
      state.listInfomation = action?.payload;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getDashboard.pending, (state) => {
        state.loading = true;
      })
      .addCase(getDashboard.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getDashboard.fulfilled, (state, action) => {
        state.success = false;
        state.ticketList = action.payload;
      });
    builder
      .addCase(getDashboardDay.pending, (state) => {
        state.loading = true;
      })
      .addCase(getDashboardDay.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getDashboardDay.fulfilled, (state, action) => {
        state.success = false;
        state.ticketList = action.payload;
      });
  }
});

export const { Infomation } = dashboardSlide.actions;

const { reducer: dashboardReducer } = dashboardSlide;
export default dashboardReducer;
