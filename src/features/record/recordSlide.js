import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axiosClient from "../../api/axiosClient";

//////initial
const initialState = {
  loading: false,
  listInfomation: {},
  urlDownload: ""
};
// Actions
const ACTION = {
  GET_RECORD: "api/getRecord",
  DOWLOAD_AUDIO: "api/getDowAudio"
};

export const getRecord = createAsyncThunk(ACTION.GET_RECORD, async (body) => {
  return axiosClient.get("/cdr/", { params: body });
});

export const getDowAudio = createAsyncThunk(
  ACTION.DOWLOAD_AUDIO,
  async (body) => {
    const config = { responseType: "blob" };
    return axiosClient.get("/download-zip/", { config, params: body });
  }
);

const recordSlide = createSlice({
  name: "recordSlide",
  initialState: initialState,
  reducers: {
    Infomation: (state, action) => {
      state.listInfomation = action?.payload;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getRecord.pending, (state) => {
        state.loading = true;
      })
      .addCase(getRecord.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getRecord.fulfilled, (state, action) => {
        state.success = false;
        // state.ticketList = action.payload;
      });

    builder
      .addCase(getDowAudio.pending, (state) => {
        state.loading = true;
      })
      .addCase(getDowAudio.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getDowAudio.fulfilled, (state, action) => {
        state.success = false;
        state.urlDownload = action.payload;
      });
  }
});

export const { Infomation } = recordSlide.actions;

const { reducer: recordReducer } = recordSlide;
export default recordReducer;
